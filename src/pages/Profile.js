import { useState, useEffect } from 'react';
import { Container, Card } from 'react-bootstrap';

export default function Profile() {

  const [profile, setProfile] = useState(null);
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => {
        setProfile(data);
      })
      .catch(error => {
        console.error("Error:", error);
      });
  }, []);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => {
        setOrders(data.orders);
      })
      .catch(error => {
        console.error("Error:", error);
      });
  }, []);

  return (
    <Container>
      <h1>Profile</h1>
      {profile && (
        <Card>
          <Card.Body>
            <Card.Title>{profile.firstName} {profile.lastName}</Card.Title>
            <Card.Subtitle>Email: {profile.email}</Card.Subtitle>
          </Card.Body>
        </Card>
      )}
      <h2>Order History</h2>
      {orders.length > 0 
      ? (orders.map(order => (
          <Card key={order._id}>
            <Card.Body>
              <Card.Title>Order ID: {order._id}</Card.Title>
              <Card.Subtitle>Date: {order.purchaseOn}</Card.Subtitle>
              <Card.Subtitle>Total: {order.totalAmount}</Card.Subtitle>
              {/* Add more order details as needed */}
            </Card.Body>
          </Card>
        ))
        )
      : 
      (
        <p>No orders found.</p>
      )}
    </Container>
  );
}
