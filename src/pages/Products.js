import { Fragment, useEffect, useState } from 'react';
// import productData from '../data/productData';
import ProductCard from '../components/ProductCard';

export default function Products() {


	const [products, setProducts] = useState([]);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product}/>
				)
			}))
		})

	}, [])

	return (
		<Fragment>
			<h1>Products</h1>
			{products}
		</Fragment>
	)
}