import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login() {

        const { user, setUser} = useContext(UserContext);

    
        const [email, setEmail] = useState('');
        const [password, setPassword] = useState('');
        
        const [isActive, setIsActive] = useState(true);


        const retrieveUserDetails = (token) => {

            fetch("${process.env.REACT_APP_API_URL}/users/details", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            });
        };


        function authenticate(e) {

            // Prevents page redirection via form submission
            e.preventDefault();

            fetch('${process.env.REACT_APP_API_URL}/users/login', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {

                console.log(data);

                // If no user information is found, the "accessToken" property will not be available and will return undefined
                if(data.accessToken !== undefined) {

                    // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
                    localStorage.setItem('token', data.accessToken);
                    retrieveUserDetails(data.accessToken);

                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })
                } else {
                    Swal.fire({
                        title: "Authentication failed!",
                        icon: "error",
                        text: "Check your log in credentials and try again!"
                    })
                }
            })
            setEmail('');
            setPassword('');

        }


        useEffect(() => {

            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);


    return (
        (user.id !== null)
        ?
        <Navigate to="/products"/>
        :
        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }

        </Form>

    )
}
